#!/usr/bin/sh

python -m venv ./bot_venv
source ./bot_venv/bin/activate
pip install -r ./src/requirements.txt
